package com.company;


public class StudyGroup {
    private String groupName;


    public StudyGroup() {
    }

    public StudyGroup(String groupName) {
        this.groupName = groupName;
    }



    @Override
    public String toString() {
        return "Group: " + groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
