package com.company;

import java.util.List;

public class SchoolMember {
    protected String name;
    protected String surname;
    protected List<StudyGroup> groups;

    public SchoolMember() {
    }

    public SchoolMember(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGroups(List<StudyGroup> groups) {
        this.groups = groups;
    }
}
