package com.company;

public class Teacher extends SchoolMember{

    @Override
    public String toString() {
        return "Teacher: " + name;
    }
}
